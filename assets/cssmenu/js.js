 $('.example-p-1').click(function () {
                                $.alert({
                                    title: 'Attention',
                                    content: 'No Data Found',
                                    confirmButton: 'Okay',
                                    confirmButtonClass: 'btn-primary',
                                    icon: 'fa fa-info',
                                    animation: 'zoom',
                                });
                            });
                            $('.konfirmasiLogout').click(function () {
                                $.confirm({
                                    title: 'Konfirmasi',
                                    content: 'You can queue confirms, you\'ll be asked again for a confirmation.',
                                    confirmButton: 'Proceed',
                                    confirmButtonClass: 'btn-info',
                                    icon: 'fa fa-question-circle',
                                    animation: 'top',
                                    confirm: function () {
                                        $.confirm({
                                            title: 'A very critical action',
                                            content: 'Are you sure you want to proceed?',
                                            confirmButton: 'Hell, yes!',
                                            icon: 'fa fa-warning',
                                            confirmButtonClass: 'btn-warning',
                                            animation: 'top',
                                            confirm: function () {
                                                document.location.href="index.php/welcome/logOut";
                                            }
                                        });
                                    }
                                });
                            });
							$('.konfirmasiBackUp').click(function () {
                                $.confirm({
                                    title: 'Konfirmasi',
                                    content: 'Backup The Data Now?',
                                    confirmButton: 'Proceed',
                                    confirmButtonClass: 'btn-info',
                                    icon: 'fa fa-question-circle',
                                    animation: 'top',
                                    confirm: function () {
                                     document.location.href="?backupdata=true";   
                                    }
                                });
                            });
                            $('#salah').click(function () {
                                $.alert({
                                    title: 'No backroundDismiss!',
                                    content: 'This feature hi-lights the dialog if the user clicks outside the dialog. <br> Else if backgroundDismiss is set to <code>TRUE</code> a cancel action is fired.',
                                    confirmButton: 'okay',
                                    confirmButtonClass: 'btn-info',
                                    animation: 'bottom',
                                    icon: 'fa fa-check',
                                    backgroundDismiss: false
                                });
                            });
                            $('.login').click(function () {
                                $.confirm({
                                    title: 'Login Here',
                                    content: 'url:index.php/welcome/logIn',
                                    confirmButton: false,
                                     cancelButton: 'Cancel',
                                    animation: 'scale',
									
                                });
                            });
							 $('.Whose_Comment').click(function () {
                                $.confirm({
                                    title: 'Who Is Comment',
                                    content: 'url:index.php/welcome/Whose_Comment',
                                    confirmButton: false,
                                     cancelButton: 'Cancel',
                                    animation: 'scale',
                                });
                            });
							 $('.Record_Pemakai').click(function () {
                                $.confirm({
                                    title: 'Record Pemakai',
                                    content: 'url:index.php/welcome/Record_Pemakai',
                                    confirmButton: false,
                                     cancelButton: 'Cancel',
                                    animation: 'scale',
                                });
                            });
							$('.gantiPassword').click(function () {
                                $.confirm({
                                    title: 'Ganti Password',
                                    content: 'url:index.php/welcome/changePassword',
                                    confirmButton: false,
                                     cancelButton: 'Cancel',
                                    animation: 'scale',
                                });
                            });
							$('.comment').click(function () {
                                $.confirm({
                                    title: 'Your Comment',
                                    content: 'url:index.php/welcome/comment',
                                    confirmButton: false,
                                     cancelButton: 'Cancel',
                                    animation: 'scale',
                                });
                            });
                            $('.tambahuser').click(function () {
                                $.confirm({
                                    title: 'Add User',
                                    content: 'url:index.php/welcome/addUser',
                                    confirmButton: false,
                                     cancelButton: 'Cancel',
                                    animation: 'scale',
                                });
                            });
							$('.seekuser').click(function () {
                                $.confirm({
                                    title: 'Seek User',
                                    content: 'url:index.php/welcome/Seek_User',
                                    confirmButton: false,
                                     cancelButton: 'Cancel',
                                    animation: 'scale',
                                });
                            });
                            $('.example-p-6').click(function () {
                                $.confirm({
                                    title: 'Need a vacation?',
                                    content: 'You have 6 seconds to make a choice',
                                    autoClose: 'cancel|6000',
                                    confirmButton: 'Yes please!',
                                    confirmButtonClass: 'btn-info',
                                    cancelButton: 'I dont need vacations',
                                    confirm: function () {
                                        alert('Vacation time!');
                                    },
                                    cancel: function () {
                                        alert('Time up!');
                                    }
                                });
								  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-53264350-2', 'auto');
          ga('send', 'pageview');
                            });
		function PesanSalah(pesan){
		    $.confirm({
                                    title: 'Confirmation',
                                    confirmButton: false,
									   content: pesan,
                                     cancelButton: 'Close',
                                    animation: 'scale',
									cancel: function () {
                                        document.location.href="";
                                    }
                                });
		}
				
		function PesanBerhasil(pesan){
		   $.alert({
                                    title: 'Attention',
                                    content: pesan,
									cancelButton: false,
                                    confirmButton: 'Okay',
                                    confirmButtonClass: 'btn-primary',
                                    icon: 'fa fa-ok',
                                    animation: 'zoom',
                                });
		}			
		function PesanBerhasilDanKeluar(pesan){
		    $.confirm({
                                    title: 'Attention',
                                    content: pesan,
								    cancelButton: false,
                                    confirmButton: 'Okay',
                                    confirmButtonClass: 'btn-primary',
                                    icon: 'fa fa-ok',
                                    animation: 'zoom',
									confirm: function () {
                                        document.location.href='index.php/welcome/LogOut';
                                    }
                                });
		}		