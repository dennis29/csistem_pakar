<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct()
	{
	parent ::__construct();
	$this->fungsi->koneksi_database();
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	$data=array();
	$this->_backupData();
	$data['index']=$this->_PotongGetIndex();
	$data['PesanBerhasilTambahuser']=$this->_SimpanUserBaru();
	$data['PesanSalahLogin']=$this->_ceklogIn();
	$data['pesanBerhasilGantiPassword']=$this->_cekBerhasilGantiPassword();
	$data['hasilPakar']=$this->_SistemPakar();
	$data['DataUntukViewGambarProfilPerusahaan']=$this->_index_simpan_gambar_ProfilPerusahaan();
	$data['DataUntukViewGambarProdukPerusahaan']=$this->_index_simpan_gambar_ProdukPerusahaan();
	$data['PesanBerhasilAddcomment']=$this->fungsi->comment();
	$data['MM_encode']=$this->session->userdata("MM_encode");
	$data['MM_id']=$this->session->userdata("MM_id");
	$data['MM_nama']=$this->session->userdata("MM_nama");
	$data['MM_pass']=$this->session->userdata("MM_pass");
	$data['MM_akses']=$this->session->userdata("MM_akses");
	$this->load->view('isi/newhome',$data);
}
function _index_simpan_gambar_ProfilPerusahaan(){
$this->fungsi->koneksi_gambar_ProfilPerusahaan();
$submit=$this->input->post('submitUpload');
if (!(empty($submit))) {
$data=array();
if ( ! $this->upload->do_upload())
{ $data['messageUntukViewGambarProfilPerusahaan'] = $this->upload->display_errors();
}
else
{
$data ['upload_data'] = $this->upload->data();
$data['messageUntukViewGambarProfilPerusahaan'] = 'Anda telah sukses mengupload gambar !!';

$config_resize = array(
'source_image' => $data['upload_data']['full_path'],
'new_image'   => './thumb/',
'maintain_ration' => true,
'width'    => 160, 
'height'    => 120
);
$this->image_lib->initialize($config_resize);
if ( ! $this->image_lib->resize())
{
$data['messageUntukViewGambarProfilPerusahaan'] = $this->image_lib->display_errors();
}
}
}
$data['images'] = $this->fungsi->fetch_image(FCPATH.'thumb');
return $data;
}
function _index_simpan_gambar_ProdukPerusahaan(){
$this->fungsi->koneksi_gambar_ProdukPerusahaan();
$submitUploadProdukPerusahaan=$this->input->post('submitUploadProdukPerusahaan');
if (!(empty($submitUploadProdukPerusahaan))) {
if ( ! $this->upload->do_upload())
{ $data['messageUntukViewGambarProdukPerusahaan'] = $this->upload->display_errors();
}
else
{
$data ['upload_data'] = $this->upload->data();
$data['messageUntukViewGambarProdukPerusahaan'] = 'Anda telah sukses mengupload gambar !!';

$config_resize = array(
'source_image' => $data['upload_data']['full_path'],
'new_image'   => './thumb/',
'maintain_ration' => true,
'width'    => 160, 
'height'    => 120
);
$this->image_lib->initialize($config_resize);
if ( ! $this->image_lib->resize())
{
$data['messageUntukViewGambarProdukPerusahaan'] = $this->image_lib->display_errors();
}
}
}
$data['images'] = $this->fungsi->fetch_image(FCPATH.'gbr');
return $data;
}
function logIn(){
$this->load->view('isi/newlogin');
}
function Whose_Comment(){
$MM_encode=$this->session->userdata("MM_encode");
if (!empty($MM_encode)){
$data=array();
$data["Whose_Comment"]=$this->fungsi->Whose_Comment();
$this->load->view('isi/Whose_Comment',$data);
}
}
function Cari_User($id_user){
$MM_encode=$this->session->userdata("MM_encode");
if (!empty($MM_encode) && !empty($id_user)){
$pesan=$this->fungsi->Cari_User($id_user);
echo $pesan;
}
}
function Record_Pemakai(){
$MM_encode=$this->session->userdata("MM_encode");
if (!empty($MM_encode)){
$data=array();
$data["Record_Pemakai"]=$this->fungsi->Record_Pemakai();
$this->load->view('isi/Record_Pemakai',$data);
}
}
function ProfilPerusahaan(){

$this->load->view('isi/newprofilperusahaan');
}
function _backupData(){
$MM_encode=$this->session->userdata("MM_encode");
$commandBackup=$this->input->get("backupdata");
if (!empty($MM_encode) && !empty($commandBackup)){
$commandBackup=$this->security->xss_clean($this->input->get("backupdata"));
$data=array();
$data=$this->db;
$this->load->view('isi/dump',$data);
}
}
function _PotongGetIndex(){
$index=$this->input->get("index");
if (!empty($index))
{
$index=$this->security->xss_clean($this->input->get("index"));
$indexTerpotong=explode(".",$index);
$newIndex=$indexTerpotong[0];
return $newIndex;
}
}
function _SistemPakar(){
$tabelpakar=$this->input->get("tabelpakar");
$notabelpakar=$this->input->get("notabelpakar");
$step=$this->input->get("step");
if (!empty($tabelpakar) && !empty($notabelpakar)){
$data=array();
$data=$this->fungsi->SistemPakar($tabelpakar,$notabelpakar,$step);
return $data;
}
}
function addUser(){
$data=array();
$data['MM_encode']=$this->session->userdata("MM_encode");
if (!empty($data['MM_encode'])){
$data['kodeBaruUser']=$this->fungsi->buatKode('sistem_admin','u-');
$this->load->view('isi/adduser',$data);
}
}
function Seek_User(){
$data['MM_encode']=$this->session->userdata("MM_encode");
if (!empty($data['MM_encode'])){
$this->load->view('isi/seek_user');
}
}
function Ubah_User(){
$data =array();
$data['MM_encode']=$this->session->userdata("MM_encode");
if (!empty($data['MM_encode'])){
$this->fungsi->Ubah_User();
echo 1;
}
}
function Hapus_User(){
$data =array();
$data['MM_encode']=$this->session->userdata("MM_encode");
if (!empty($data['MM_encode'])){
$this->fungsi->Hapus_User();
echo 1;
}
}
function comment(){
$data=array();
$data['MM_encode']=$this->session->userdata("MM_encode");
if (!empty($data['MM_encode'])){
$this->load->view('isi/comment',$data);
}
}
function changePassword(){
$MM_pass=$this->session->userdata("MM_pass");
if (!empty($MM_pass)){
$this->load->view('isi/gantipassword');
}
}
function _SimpanUserBaru(){
	$iduseruntukaddadmin=$this->input->post("iduseruntukaddadmin");
    $namauntukaddadmin=$this->input->post("namauntukaddadmin");
	$passworduntukaddadmin=$this->input->post("passworduntukaddadmin");
	if ((!empty($iduseruntukaddadmin))&&(!empty($namauntukaddadmin)) &&(!empty($passworduntukaddadmin))){ 
	$iduseruntukaddadmin=$this->security->xss_clean($this->input->post("iduseruntukaddadmin"));
    $namauntukaddadmin=$this->security->xss_clean($this->input->post("namauntukaddadmin"));
	$passworduntukaddadmin=$this->security->xss_clean($this->input->post("passworduntukaddadmin"));
	  $PesanBerhasilTambahuser=$this->fungsi->addUser('sistem_admin', $iduseruntukaddadmin,$namauntukaddadmin,$passworduntukaddadmin);
	  return $PesanBerhasilTambahuser;
	}
}
function _cekBerhasilGantiPassword(){
	$passwordbaruuntukgantipassword=$this->input->post("passwordbaruuntukgantipassword");
	$MM_id=$this->session->userdata("MM_id");
	if ((!empty($passwordbaruuntukgantipassword))&&(!empty($MM_id)) ){ 
	$passwordbaruuntukgantipassword=$this->security->xss_clean($this->input->post("passwordbaruuntukgantipassword"));
	  $this->fungsi->GantiPassword($passwordbaruuntukgantipassword,$MM_id);
	 return $PesanBerhasilGantiPassword="true"; 	 
	}
}
function logOut(){
$MM_encode=$this->session->userdata("MM_encode");
if (!empty($MM_encode)){
$this->session->sess_destroy();
redirect(base_url());
}
}
function _ceklogIn(){
	$inputid=$this->input->post("userid");
    $inputpass=$this->input->post("password");
	if ((!empty($inputid))&&(!empty($inputpass))){
	$inputid=$this->security->xss_clean($this->input->post("userid"));
    $inputpass=$this->security->xss_clean($this->input->post("password"));
	$hslQry=$this->db->select("*")->where("userid",$inputid)->where("password",$inputpass)->get("sistem_admin");
	if ($hslQry->num_rows() > 0){
	$row=$hslQry->result_array();
	$MM_nama=$row[0]['nama'];
	$MM_akses=$row[0]["hakakses"];
	$this->session->set_userdata("MM_encode",md5($inputid));
	$this->session->set_userdata("MM_id",$inputid);
	$this->session->set_userdata("MM_nama",$MM_nama);
	$this->session->set_userdata("MM_pass",$inputpass);
	$this->session->set_userdata("MM_akses",$MM_akses);  
	redirect(base_url());
	}
	 else return $PesanSalahLogin="true"; 	 
	 }
}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */