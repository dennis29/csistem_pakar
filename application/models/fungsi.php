<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Fungsi extends CI_Model
{
function koneksi_database()
{
$config['hostname']="localhost";
$config['dbdriver']="mysql";
$config['database']="db_sistem_pakar";
$config['username']="root";
$config['password']="";
$this->load->database($config);
}
function koneksi_gambar_ProfilPerusahaan(){
$config['upload_path'] = './thumb';
$config['allowed_types']= 'gif|jpg|png';
$config['max_size'] = '1024';
$config['max_width'] = '900';
$config['max_height'] = '300';
$this->upload->initialize($config);
}
function koneksi_gambar_ProdukPerusahaan(){
$config['upload_path'] = './gbr';
$this->upload->initialize($config);
}
function fetch_image($path)
{
$this->load->helper('file');
return get_filenames($path);
}

function cekcaptcha($captcha,$sessioncapthca)
{
        $cap = 'wrong';
			echo $cap;
}
function comment()
{
$comment=$this->input->post("txtcomment");
if (!empty($comment)){
$this->db->set('id',$this->session->userdata('MM_id'))->set('comment',$comment)->insert("tabel_comment");
return "True";
}
}
function SistemPakar($tabelpakar,$notabelpakar,$step)
{
$data=array();
$hasilTanya=$this->db->select("*")->where("id_pakar",$tabelpakar)->where("no",$notabelpakar)->get("tabel_pakar");
$rowHasil=$hasilTanya->result_array();
if ($rowHasil[0]['jenis']=="jump"){
$data['gbrpakar']=$rowHasil[0]['isi'];
$data['nobenar']=$rowHasil[0]['isi'].".1";
$data['nosalah']=$rowHasil[0]['isi'].".2";
$tanya=$this->db->select("*")->where("id_pakar",$tabelpakar)->where("no",$rowHasil[0]['isi'])->get("tabel_pakar");
$rowHasil=$tanya->result_array();
}
else{
$data['nobenar']=$notabelpakar.".1";
$data['nosalah']=$notabelpakar.".2";
$data['gbrpakar']=$notabelpakar;
}
if (($rowHasil[0]['jenis']=="solusi")||($rowHasil[0]['jenis']=="final")){
$datainsert = array(
				 'id_model' => $rowHasil[0]['id_pakar']
				, 'id_pemakai' => $this->session->userdata("MM_id")
				, 'tanggal' => date("Y-m-d")
			);
$this->db->insert("tabel_record_pemakai", $datainsert);
}
$data['isi']=$rowHasil[0]['isi'];
$data['jenis']=$rowHasil[0]['jenis'];
$data['tabelpakar']=$tabelpakar;
$data['step']=$step;
return $data;
}
function addUser($table,$iduseruntukaddadmin,$namauntukaddadmin,$passworduntukaddadmin)
{
$datainsert = array(
				 'userid' => $iduseruntukaddadmin
				, 'nama' => $namauntukaddadmin
				, 'password' => $passworduntukaddadmin
				, 'hakakses' => "0"	
			);
$this->db->insert($table, $datainsert);
return $PesanBerhasilTambahuser="true"; 	 
}
function GantiPassword($password,$iduser)
{
$this->db->set("password",$password)->where("userid",$iduser)->update("sistem_admin");
}
function buatKode($tabel, $inisial)
{
	$struktur	= mysql_query("SELECT * FROM $tabel");
	$field		= mysql_field_name($struktur,0);
	//$panjang	= mysql_field_len($struktur,0);

 	$qry	= mysql_query("SELECT MAX(".$field.") FROM ".$tabel);
 	$row	= mysql_fetch_array($qry); 
 	if ($row[0]=="") {
 		$angka=0;
	}
 	else {
 		$angka		= substr($row[0], strlen($inisial));
 	}
	
 	$angka++;
 	$angka	=strval($angka); 
 	$tmp	="";
 	for($i=1; $i<=('5'-strlen($inisial)-strlen($angka)); $i++) {
		$tmp=$tmp."0";	
	}
 	return $inisial.$tmp.$angka;
}
function Whose_Comment()
{
return $this->db->select("*")->from("tabel_comment")->join("sistem_admin","sistem_admin.userid=tabel_comment.id")->order_by("tabel_comment.id")->get();
}
function Ubah_User(){
$this->db->set("nama",$_REQUEST['namauntukseekuser'])->set("password",$_REQUEST['passworduntukseekuser'])->where("userid",$_REQUEST['iduntukseekuser'])->update("sistem_admin");
}
function Hapus_User(){
$this->db->where("userid",$_REQUEST['iduntukseekuser'])->delete("sistem_admin");
}
function Cari_User($id_user='')
{
$hslQuery = $this->db->select("*")->from("sistem_admin")->where("userid",$id_user)->where("hakakses","0")->get();
$rowHasil=$hslQuery->result_array();
if ($rowHasil){
return $rowHasil[0]['nama']."@*&@*#&**&#^^^#^@*".$rowHasil[0]['password'];
}
else
{
return 0;
}
}
function Record_Pemakai()
{
return $this->db->select("*")->from("tabel_record_pemakai")->join("sistem_admin","sistem_admin.userid=tabel_record_pemakai.id_pemakai")->join("tabel_model_pakar","tabel_model_pakar.id=tabel_record_pemakai.id_model")->order_by("tabel_record_pemakai.tanggal")->get();
}

}